package com.airic.lunar;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import com.airic.lunar.util.Lunar;
import com.airic.lunar.util.LunarUtil;
import com.airic.lunar.util.Solar;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

/**
 * The Class LunarUtilTest.
 */
public class LunarUtilTest {

	/**
	 * Test solar to lunar leap.
	 */
	@Test
	public void testSolarToLunarLeap() {
		// 公曆轉農曆
		System.out.println("---------------------");
		System.out.println("公曆轉農曆:");
		System.out.println("---------------------");
		{
			System.out.println("Example 1: 公曆1990年6月23日轉農曆");
			Lunar lunar = LunarUtil.solar2Lunar(1990, 6, 23).get();
			System.out.println(lunar.toString());
			System.out.println("年: " + lunar.getYear() + ", 月: " + lunar.getMonth() + ", 日: " + lunar.getDay() + ", 閏月: "
					+ (lunar.isLeap() ? "是" : "否"));
			System.out.println();

			Assertions.assertNotNull(lunar);
			Assertions.assertEquals(1990, lunar.getYear());
			Assertions.assertEquals(5, lunar.getMonth());
			Assertions.assertEquals(1, lunar.getDay());
			Assertions.assertEquals(true, lunar.isLeap());
		}
		{
			System.out.println("Example 2: 公曆2033年12月22日轉農曆");
			Lunar lunar = LunarUtil.solar2Lunar(2033, 12, 22).get();
			System.out.println(lunar.toString());
			System.out.println("年: " + lunar.getYear() + ", 月: " + lunar.getMonth() + ", 日: " + lunar.getDay() + ", 閏月: "
					+ (lunar.isLeap() ? "是" : "否"));
			System.out.println("閏月: " + (lunar.isLeap() ? "是" : "否"));
			System.out.println();

			Assertions.assertNotNull(lunar);
			Assertions.assertEquals(2033, lunar.getYear());
			Assertions.assertEquals(11, lunar.getMonth());
			Assertions.assertEquals(1, lunar.getDay());
			Assertions.assertEquals(true, lunar.isLeap());
		}
	}

	/**
	 * Test lunar leap to solar.
	 */
	@Test
	public void testLunarLeapToSolar() {
		// 農曆轉公曆
		System.out.println("---------------------");
		System.out.println("農曆轉公曆:");
		System.out.println("---------------------");
		{
			Solar solar = LunarUtil.lunar2Solar(1990, 5, 1, true).get();
			System.out.println("Example 1: 農曆1990年閏五月初一轉公曆");
			System.out.println(solar.toString());
			System.out.println("年: " + solar.getYear() + ", 月: " + solar.getMonth() + ", 日: " + solar.getDay());
			System.out.println();

			Assertions.assertNotNull(solar);
			Assertions.assertEquals(1990, solar.getYear());
			Assertions.assertEquals(6, solar.getMonth());
			Assertions.assertEquals(23, solar.getDay());
		}
		{
			Solar solar = LunarUtil.lunar2Solar(2033, 11, 1, true).get();
			System.out.println("Example 2: 農曆2033年閏十一月初一轉公曆");
			System.out.println(solar.toString());
			System.out.println("年: " + solar.getYear() + ", 月: " + solar.getMonth() + ", 日: " + solar.getDay());
			System.out.println();

			Assertions.assertNotNull(solar);
			Assertions.assertEquals(2033, solar.getYear());
			Assertions.assertEquals(12, solar.getMonth());
			Assertions.assertEquals(22, solar.getDay());
		}
	}

	/**
	 * Test convert max lunar date.
	 */
	@Test
	public void testConvertMaxLunarDate() {
		Lunar lunar = new Lunar(LunarUtil.LUNAR_MAX_DATE.getYear(), LunarUtil.LUNAR_MAX_DATE.getMonthOfYear(),
				LunarUtil.LUNAR_MAX_DATE.getDayOfMonth(), false);
		Solar solar = LunarUtil.lunar2Solar(lunar.getYear(), lunar.getMonth(), lunar.getDay(), lunar.isLeap()).orElse(null);

		Assertions.assertEquals("2099(己未)年臘月三十", lunar.toString());
		Assertions.assertEquals("2100-2-8", solar.toString());
	}

	/**
	 * Test lunar2 solar2 lunar date.
	 */
	@Test
	public void testLunar2Solar2LunarDate() {
		boolean success = true;
		String wrongDateMessage = "";
		/*
		 * 農曆西曆互相轉換
		 */
		for (int year = LunarUtil.LUNAR_MIN_DATE.getYear(); year < LunarUtil.LUNAR_MAX_DATE.getYear(); year++) {
			for (int month = 1; month <= 12; month++) {
				for (int day = 1; day <= 31; day++) {
					Lunar lunar = new Lunar(year, month, day, false);
					if (LunarUtil.testDateInputValidRange(lunar)) {
						Solar solarDate = LunarUtil.lunar2Solar(year, month, day, false).orElse(null);
						if (solarDate != null && LunarUtil.testDateInputValidRange(solarDate)) {
							Lunar lunarDate = LunarUtil.solar2Lunar(solarDate.getYear(), solarDate.getMonth(), solarDate.getDay()).get();
							if (lunarDate.getYear() != year || lunarDate.getMonth() != month || lunarDate.getDay() != day
									|| lunarDate.isLeap()) {
								wrongDateMessage = "incorrect conversion of lunar->solar->lunar: " + year + " " + month + " " + day;
								System.out.println(wrongDateMessage);
								success = false;
								break;
							}
						}
					}
				}
			}
		}

		Assertions.assertTrue(success, wrongDateMessage);
	}

	/**
	 * Test solar2 lunar2 solar date.
	 */
	@Test
	public void testSolar2Lunar2SolarDate() {
		boolean success = true;
		String wrongDateMessage = "";
		/*
		 * 農曆西曆互相轉換
		 */
		for (int year = LunarUtil.LUNAR_MIN_DATE.getYear(); year < LunarUtil.LUNAR_MAX_DATE.getYear(); year++) {
			for (int month = 1; month <= 12; month++) {
				for (int day = 1; day <= 31; day++) {
					Lunar lunar = new Lunar(year, month, day, false);
					if (LunarUtil.testDateInputValidRange(lunar)) {
						Solar solar = new Solar(year, month, day);
						if (LunarUtil.testDateInputValidRange(solar)) {
							Lunar lunarDate = LunarUtil.solar2Lunar(year, month, day).orElse(null);
							if (lunarDate != null && LunarUtil.testDateInputValidRange(lunarDate)) {
								Solar solarDate = LunarUtil.lunar2Solar(lunarDate.getYear(), lunarDate.getMonth(), lunarDate.getDay(),
										lunarDate.isLeap()).get();
								if (solarDate.getYear() != year || solarDate.getMonth() != month || solarDate.getDay() != day) {
									wrongDateMessage = "incorrect conversion of solar->lunar->solar: " + year + " " + month + " " + day;
									System.out.println(wrongDateMessage);
									success = false;
									break;
								}
							}
						}
					}
				}
			}
		}

		Assertions.assertTrue(success, wrongDateMessage);
	}

	/**
	 * Test special day cases.
	 */
	@Test
	public void testSpecialDayCases() {

		{
			// test農曆1990-1-30並不存在，是錯誤日子
			Solar solar = LunarUtil.lunar2Solar(1990, 1, 30, false).orElse(null);
			Assertions.assertNull(solar);
			if (solar != null) {
				System.out.println("lunar2Solar incorrect: lunar 1901-1-30 should return null as it is invalid input.");
			}
		}

		{
			// test西曆1990-6-23->農曆閏五月初一
			Lunar lunar = LunarUtil.solar2Lunar(1990, 6, 23).orElse(null);
			Assertions.assertNotNull(lunar);
			Assertions.assertEquals(1990, lunar.getYear());
			Assertions.assertEquals(5, lunar.getMonth());
			Assertions.assertEquals(1, lunar.getDay());
			Assertions.assertEquals(true, lunar.isLeap());
		}

		{
			// test農曆閏四月初一並不存在，是錯誤日子
			Solar solar = LunarUtil.lunar2Solar(1990, 4, 1, true).orElse(null);
			Assertions.assertNull(solar);
		}

		{
			// test西曆1996-7-15->農曆五月三十
			Lunar lunar = LunarUtil.solar2Lunar(1996, 7, 15).orElse(null);
			Assertions.assertNotNull(lunar);
			Assertions.assertEquals(1996, lunar.getYear());
			Assertions.assertEquals(5, lunar.getMonth());
			Assertions.assertEquals(30, lunar.getDay());
			Assertions.assertEquals(false, lunar.isLeap());
		}

		{
			// test西曆1996-8-14->農曆七月初一
			Lunar lunar = LunarUtil.solar2Lunar(1996, 8, 14).orElse(null);
			Assertions.assertNotNull(lunar);
			Assertions.assertEquals(1996, lunar.getYear());
			Assertions.assertEquals(7, lunar.getMonth());
			Assertions.assertEquals(1, lunar.getDay());
			Assertions.assertEquals(false, lunar.isLeap());
		}

		{
			// test農曆閏六月三十日->西曆2017-8-21
			Solar solar = LunarUtil.lunar2Solar(2017, 6, 30, true).orElse(null);
			Assertions.assertNotNull(solar);
			Assertions.assertEquals(2017, solar.getYear());
			Assertions.assertEquals(8, solar.getMonth());
			Assertions.assertEquals(21, solar.getDay());
		}

		{
			// test西曆2017-8-15->農曆閏六月廿四日
			Lunar lunar = LunarUtil.solar2Lunar(2017, 8, 15).orElse(null);
			Assertions.assertNotNull(lunar);
			Assertions.assertEquals(2017, lunar.getYear());
			Assertions.assertEquals(6, lunar.getMonth());
			Assertions.assertEquals(24, lunar.getDay());
			Assertions.assertEquals(true, lunar.isLeap());
		}

		{
			// test西曆2033-8-25->農曆八月初一，測試2033置閏問題
			Lunar lunar = LunarUtil.solar2Lunar(2033, 8, 25).orElse(null);
			Assertions.assertNotNull(lunar);
			Assertions.assertEquals(2033, lunar.getYear());
			Assertions.assertEquals(8, lunar.getMonth());
			Assertions.assertEquals(1, lunar.getDay());
			Assertions.assertEquals(false, lunar.isLeap());
		}

		{
			// test西曆2033-12-22->農曆閨十一月初一，測試2033置閏問題
			Lunar lunar = LunarUtil.solar2Lunar(2033, 12, 22).orElse(null);
			Assertions.assertNotNull(lunar);
			Assertions.assertEquals(2033, lunar.getYear());
			Assertions.assertEquals(11, lunar.getMonth());
			Assertions.assertEquals(1, lunar.getDay());
			Assertions.assertEquals(true, lunar.isLeap());
		}

		{
			// test西曆2033-9-23->農曆九月初一，測試2033置閏問題
			Lunar lunar = LunarUtil.solar2Lunar(2033, 9, 23).orElse(null);
			Assertions.assertNotNull(lunar);
			Assertions.assertEquals(2033, lunar.getYear());
			Assertions.assertEquals(9, lunar.getMonth());
			Assertions.assertEquals(1, lunar.getDay());
			Assertions.assertEquals(false, lunar.isLeap());
		}
	}

	@Test
	public void testDataSheetDatesTestCases() throws IOException {
		try (BufferedReader br = new BufferedReader(new FileReader(this.getClass().getClassLoader().getResource("verifyDateLookUp.csv").getFile()))) {
			String line;
			while ((line = br.readLine()) != null) {
				String solarDate = line.split(",")[0];
				int solarYear = Integer.parseInt(solarDate.split("-")[0]);
				int solarMonth = Integer.parseInt(solarDate.split("-")[1]);
				int solarDay = Integer.parseInt(solarDate.split("-")[2]);

				String lunarDate = line.split(",")[1];
				boolean isLeap = false;
				if (lunarDate.contains("*")){
					isLeap = true;
					lunarDate = lunarDate.replace("*", "");
				}
				int lunarYear = Integer.parseInt(lunarDate.split("-")[0]);
				int lunarMonth = Integer.parseInt(lunarDate.split("-")[1]);
				int lunarDay = Integer.parseInt(lunarDate.split("-")[2]);

				{
					Lunar lunar = LunarUtil.solar2Lunar(solarYear, solarMonth, solarDay).orElse(null);
					String lunarResultDateString = lunar.getYear() + "-" + lunar.getMonth() + "-" + lunar.getDay() + (lunar.isLeap() ? "*" : "");
					Assertions.assertEquals(
							new Lunar(lunarYear, lunarMonth, lunarDay, isLeap), lunar,
							String.format("solar2Lunar incorrect: solar %s should be lunar %s instead of %s", solarDate, lunarDate, lunarResultDateString));
				}

				{
					Solar solar = LunarUtil.lunar2Solar(lunarYear, lunarMonth, lunarDay, isLeap).orElse(null);
					String solarResultDateString = solar.getYear() + "-" + solar.getMonth() + "-" + solar.getDay();
					Assertions.assertEquals(
							new Solar(solarYear, solarMonth, solarDay), solar,
							String.format("lunar2Solar incorrect: lunar %s should be solar %s instead of %s", lunarDate, solarDate, solarResultDateString));
				}

			}
		}
	}
}