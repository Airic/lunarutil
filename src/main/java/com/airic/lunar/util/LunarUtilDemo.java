package com.airic.lunar.util;

/**
 * The Class LunarUtilTest.
 */
public class LunarUtilDemo {

	/**
	 * The main method.
	 *
	 * @param args
	 *            the arguments
	 */
	public static void main(String[] args) {
		runExample();
	}

	/**
	 * Run example.
	 */
	public static void runExample() {
		// 公曆轉農曆
		System.out.println("---------------------");
		System.out.println("公曆轉農曆:");
		System.out.println("---------------------");
		{
			System.out.println("Example 1: 公曆1990年6月23日轉農曆");
			Lunar lunar = LunarUtil.solar2Lunar(1990, 6, 23).get();
			System.out.println(lunar.toString());
			System.out.println("年: " + lunar.getYear() + ", 月: " + lunar.getMonth() + ", 日: " + lunar.getDay() + ", 閏月: "
			        + (lunar.isLeap() ? "是" : "否"));
			System.out.println();
		}
		{
			System.out.println("Example 2: 公曆2033年12月22日轉農曆");
			Lunar lunar = LunarUtil.solar2Lunar(2033, 12, 22).get();
			System.out.println(lunar.toString());
			System.out.println("年: " + lunar.getYear() + ", 月: " + lunar.getMonth() + ", 日: " + lunar.getDay() + ", 閏月: "
			        + (lunar.isLeap() ? "是" : "否"));
			System.out.println("閏月: " + (lunar.isLeap() ? "是" : "否"));
			System.out.println();
		}

		// 農曆轉公曆
		System.out.println("---------------------");
		System.out.println("農曆轉公曆:");
		System.out.println("---------------------");
		{
			Solar solar = LunarUtil.lunar2Solar(1990, 5, 1, true).get();
			System.out.println("Example 1: 農曆1990年閏五月初一轉公曆");
			System.out.println(solar.toString());
			System.out.println("年: " + solar.getYear() + ", 月: " + solar.getMonth() + ", 日: " + solar.getDay());
			System.out.println();
		}
		{
			Solar solar = LunarUtil.lunar2Solar(2033, 11, 1, true).get();
			System.out.println("Example 2: 農曆2033年閏十一月初一轉公曆");
			System.out.println(solar.toString());
			System.out.println("年: " + solar.getYear() + ", 月: " + solar.getMonth() + ", 日: " + solar.getDay());
			System.out.println();
		}
	}

}
